package com.example.sander.fontysmon;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class StartActivity extends AppCompatActivity {

    ObjectAnimator animation;
    boolean logoIsBottom = false;

    int door = 0;
    ImageView doorImageView;
    ImageView teacherImgView;
    TextView startText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        Button continueBtn = findViewById(R.id.continueBtn);
        final Storage storage = new Storage(this);


        final ImageView logoImgView = findViewById(R.id.logoImgView);
        teacherImgView = findViewById(R.id.teacherImageView);

        doorImageView = findViewById(R.id.doorImageView);
        startText = findViewById(R.id.startText);

        SpriteCharacter sebastiaan = new SpriteCharacter(this, R.drawable.sprite_seb, "sebastiaan");
        teacherImgView.setBackground(new BitmapDrawable(sebastiaan.getSprite(SpriteCharacter.SpriteState.DownNormal)));

        final Handler logoHandler = new Handler();
        final Handler doorHandler = new Handler();

        logoHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(logoIsBottom){
                    animation = ObjectAnimator.ofFloat(startText, "alpha", 1f);
                    logoIsBottom = false;
                }
                else{
                    animation = ObjectAnimator.ofFloat(startText, "alpha", 0.2f);
                    logoIsBottom = true;
                }

                animation.setDuration(1000);
                animation.start();
                logoHandler.postDelayed(this, 1000);
            }
        }, 0);



        for (int a = 0; a < 6 ;a++) {
            doorHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    door++;
                    switch(door){
                        case 1:
                            doorImageView.setImageResource(R.drawable.door1);
                            break;
                        case 2:
                            doorImageView.setImageResource(R.drawable.door2);
                            break;
                        case 3:
                            doorImageView.setImageResource(R.drawable.door3);
                            break;
                        case 4:
                            doorImageView.setImageResource(R.drawable.door4);
                            break;
                        case 5:
                            doorImageView.setImageResource(R.drawable.door5);
                            System.out.println("FIVE!");
                            break;
                        case 6:
                            teacherImgView.setVisibility(View.VISIBLE);
                            System.out.println("SIX!");
                            break;

                    }

                }
            }, 400 * a);
        }




        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonSound();
                if (storage.load("Name", "null") == "null") {
                    System.out.println("loading user creation");
                    startActivity(new Intent(StartActivity.this, UserCreationActivity.class));
                    finish();
                }
                else{
                    System.out.println("User exists, continuing to game");
                    startActivity(new Intent(StartActivity.this, MapActivity.class));
                    finish();
                }
            }
        });

    }

    public void buttonSound(){
        Sound.play(this, Sound.Effect.ButtonClick);
    }

}
