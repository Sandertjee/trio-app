package com.example.sander.fontysmon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UserCreationActivity extends AppCompatActivity {

    int imageMargin = -99;
    ImageView ook;
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_creation);

        final Button submitBtn = findViewById(R.id.submitBtn);
        final Button confirmBtn = findViewById(R.id.confirmBtn);
        final Button declineBtn = findViewById(R.id.declineBtn);
        final EditText nameEditText = findViewById(R.id.nameEditText);
        final LinearLayout buttonContainer = findViewById(R.id.buttonContainer);
        final LinearLayout confirmContainer = findViewById(R.id.confirmContainer);
        final TextView welcomeMsgTextView = findViewById(R.id.welcomeMessageTextView);

        ook = findViewById(R.id.imgSebastiaan);

        final Storage storage = new Storage(this);


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonSound();
                String name = nameEditText.getText().toString();

                if(name.equals("")){
                    name = "Wum";
                }

                submitBtn.setVisibility(View.INVISIBLE);
                welcomeMsgTextView.setText("So your name is " + name + "? Is this correct?");
                nameEditText.setVisibility(View.INVISIBLE);
                buttonContainer.setVisibility(View.INVISIBLE);
                confirmContainer.setVisibility(View.VISIBLE);

                confirmBtn.setVisibility(View.VISIBLE);
                declineBtn.setVisibility(View.VISIBLE);
            }
        });

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonSound();

                String name = nameEditText.getText().toString();

                if(name.equals("")){
                    name = "Wum";
                }

                storage.save("Name", name );
                storage.save("Level", 1);
                storage.save("Health", 100);
                storage.save("Sprite", null);
                storage.save("Methods", String.join(",", Attack.ID.LiteratureStudy.toString(), Attack.ID.PaperPrototype.toString(), Attack.ID.UsabilityTest.toString(), Attack.ID.PoC.toString()));
                storage.save("Badges", String.join(",", ""));
                storage.save("Xposition", 0);
                storage.save("Yposition", 0);
                startActivity(new Intent(UserCreationActivity.this, MapActivity.class));
                finish();
            }
        });

        declineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonSound();
                nameEditText.setText("");

                welcomeMsgTextView.setText("Welcome to Fontysmon! What is your name?");

                submitBtn.setVisibility(View.VISIBLE);
                nameEditText.setVisibility(View.VISIBLE);

                buttonContainer.setVisibility(View.VISIBLE);
                confirmContainer.setVisibility(View.INVISIBLE);

                confirmBtn.setVisibility(View.INVISIBLE);
                declineBtn.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void buttonSound(){
        Sound.play(this, Sound.Effect.ButtonClick);
    }
}
