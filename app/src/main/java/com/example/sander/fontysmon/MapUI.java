package com.example.sander.fontysmon;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Handler;
import android.view.Display;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.Map;

public class MapUI {

    private static final int EMPTY_ID = 0, PLAYER_ID = 999;
    public static final int ANIMATION_SPEED = 400;

    private Activity activity;
    private RelativeLayout upper_layout, sprite_layout;
    private GridLayout tiles_layout;
    private SpriteCharacter player;
    private int rows, cols, canvas_width, canvas_height;

    private Map<Integer, Tile> tiles;
    private Tile player_tile;

    public MapUI(Activity activity, RelativeLayout layout, int rows, int cols) {

        // put values into properties
        this.activity = activity;
        this.upper_layout = layout;
        this.rows = rows;
        this.cols = cols;

        // Layout params for both layout fields
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Create new layout for the floor grid
        tiles_layout = new GridLayout(activity);
        tiles_layout.setLayoutParams(params);
        upper_layout.addView(tiles_layout);

        // Create layout for the sprite to walk on
        sprite_layout = new RelativeLayout(activity);
        sprite_layout.setLayoutParams(params);
        upper_layout.addView(sprite_layout);

//      New sprite character for the player
        player = new SpriteCharacter(activity, R.drawable.sprite_player, "You");

        initMap();
    }

    private void initMap() {
        tiles = new HashMap<>();

        // Get screen dimensions
        Display dm = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        dm.getSize(size);
        int screen_width = size.x;

        // Calculate a grid layout height from screen width and tile dimensions
        Tile.setSize(screen_width / cols);
        tiles_layout.setColumnCount(cols);
        tiles_layout.setRowCount(rows);

        canvas_width = cols * Tile.TILE_WIDTH;
        canvas_height = rows * Tile.TILE_HEIGHT;

        // Set width and height to the player layout grid
        RelativeLayout.LayoutParams r_params = new RelativeLayout.LayoutParams(canvas_width, canvas_height);
        sprite_layout.setLayoutParams(r_params);
//        relative_layout.setBackgroundColor(Color.CYAN);

        // This boolean is to create a checkered pattern on de floor grid (before custom textures are attached to keep the tile apart)
        boolean black;

        // Default Tile to refer to if something goes wrong (a tile is always needed is another tile cannot be found)
        tiles.put(EMPTY_ID, new Tile(activity, 0, 0,0));
        int counter = 1;

        // build rows
        for (int row = 1; row < rows + 1; row++) {

            // build columns
            for (int col = 1; col < cols + 1; col++) {

                // Create custom ID
                int ID = counter;
                System.out.println("Tile: #" + ID + " (" + row + "x" + col + ")");

                // New Tile
                Tile tile = new Tile(activity, ID, col, row);

                // Little calculation to decide if the tile should be black of white
                black = ((col + row) % 2 == 0);
                int back = (black) ? activity.getColor(R.color.white) : activity.getColor(R.color.tile_2);
                tile.setBackgroundColor(back);

                // Define position of the tile on the GridLayout
                GridLayout.LayoutParams g_params = new GridLayout.LayoutParams();
                g_params.rowSpec = GridLayout.spec(row - 1);
                g_params.columnSpec = GridLayout.spec(col - 1);
                tile.setLayoutParams(g_params);

                // Attach OnClickListener from Custom activity

                // Add Tile to the Tile Map
                tiles.put(tile.getID(), tile);

                counter++;
            }
        }

        // Add all tiles to the GridLayout
        // For safety reasons this happens apart from the default for-loop in case some additional changes have to be made
        for (Tile tile : tiles.values()) {
            System.out.println(tile.getLocation().toString() + "==============================");
            tiles_layout.addView(tile);
        }

        System.out.println("GRID (" + rows + "x" + cols + ") ==============================");

        // Create player tile
        player_tile = new Tile(activity, PLAYER_ID, 1, 1);
        tiles.put(player_tile.getID(), player_tile);
        sprite_layout.addView(player_tile);

        // Reset walking permission
        player.stopWalking();

        // Set walking valuess
        initWalking(player, player_tile);

        player_tile.resetImage();
    }

    private void initWalking(final SpriteCharacter character, final Tile character_tile) {
        System.out.println("===================== INIT WALKING");

        final int[] is_walk_1 = {1};

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Bitmap normal, walk_1, walk_2;

                    // Switch to determine which direction the sprite is looking
                    switch (character.getDirection()) {
                        case Up:
                            normal = character.getSprite(SpriteCharacter.SpriteState.UpNormal);
                            walk_1 = character.getSprite(SpriteCharacter.SpriteState.UpWalk1);
                            walk_2 = character.getSprite(SpriteCharacter.SpriteState.UpWalk2);
                            break;
                        default:
                        case Down:
                            normal = character.getSprite(SpriteCharacter.SpriteState.DownNormal);
                            walk_1 = character.getSprite(SpriteCharacter.SpriteState.DownWalk1);
                            walk_2 = character.getSprite(SpriteCharacter.SpriteState.DownWalk2);
                            break;
                        case Left:
                            normal = character.getSprite(SpriteCharacter.SpriteState.LeftNormal);
                            walk_1 = character.getSprite(SpriteCharacter.SpriteState.LeftWalk1);
                            walk_2 = character.getSprite(SpriteCharacter.SpriteState.LeftWalk2);
                            break;
                        case Right:
                            normal = character.getSprite(SpriteCharacter.SpriteState.RightNormal);
                            walk_1 = character.getSprite(SpriteCharacter.SpriteState.RightWalk1);
                            walk_2 = character.getSprite(SpriteCharacter.SpriteState.RightWalk2);
                            break;
                    }

                    // Define which image to use when walking or standing still
                    // Handler used to create a walking effect when the sprite is walking
                    if (character.isWalking()) {
                        if (is_walk_1[0] == 1) {
                            character_tile.setImage(walk_1);
                            is_walk_1[0] = 2;
                        } else {
                            character_tile.setImage(walk_2);
                            is_walk_1[0] = 1;
                        }
                        System.out.println("Walk animation");
                    } else {
                        character_tile.setImage(normal);
                    }
                    handler.postDelayed(this, 200);
                }
            }, 0);

            // Other handler for walking from one tile to another
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (character.isWalking()) {
                        System.out.println("Walking on tile");
                        System.out.println(character_tile.getX() + " x " + character_tile.getY());
                        switch (character.getDirection()) {
                            case Up:
                                doWalk(character, character_tile, SpriteCharacter.State.Up);
                                break;
                            case Down:
                                doWalk(character, character_tile, SpriteCharacter.State.Down);
                                break;
                            case Left:
                                doWalk(character, character_tile, SpriteCharacter.State.Left);
                                break;
                            case Right:
                                doWalk(character, character_tile, SpriteCharacter.State.Right);
                                break;
                            default:
                                break;

                        }
                    }
                    handler.postDelayed(this, ANIMATION_SPEED);
                }
            }, 0);
    }

    /* PLAYER METHODS
    *****************************************************************/
    public Tile getPlayerTile() {
        return tiles.get(PLAYER_ID);
    }

    private void setPlayerTile(Tile tile){
        player_tile = tile;
        tiles.replace(PLAYER_ID, tile);
    }

    public SpriteCharacter getPlayerSprite() {
        return player;
    }

    public void setPlayerDirection(SpriteCharacter.State state){
        player.setDirection(state);
    }

    public Map<Integer, Tile> getTiles() {
        return tiles;
    }

    public Tile getTile(int id){
        return tiles.get(id);
    }

    public Activity getActivity() {
        return activity;
    }

    public Tile find(int x, int y, Tile current_tile, boolean force){
        for (Tile tile : tiles.values()){
            if (tile.getLocation().compare(x, y) && (!tile.isBlocked() || force)){
                return tiles.get(tile.getId());
            }
        }
        return current_tile;
    }

    public Tile find(int x, int y, boolean force){
        return find(x, y, tiles.get(EMPTY_ID), force);
    }

    public Tile find(int x, int y, Tile current_tile){
        return find(x, y, current_tile, false);
    }

    public Tile find(int x, int y){
        return find(x, y, tiles.get(EMPTY_ID), false);
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public void doWalk(SpriteCharacter sprite, Tile sprite_tile, SpriteCharacter.State state) {
        sprite.setDirection(state);
        TileLocation location = sprite_tile.getLocation();
        int x = location.getX(), y = location.getY();

        switch (state){
            case Up:
                y = y - 1;
                break;
            case Down:
                y = y + 1;
                break;
            case Left:
                x = x - 1;
                break;
            case Right:
                x = x + 1;
                break;
        }
        System.out.println(new TileLocation(x, y).toString());
        if (x >= 1 && y >= 1 && x <= cols && y <= rows) {
            Tile new_location = find(x, y, sprite_tile);
            if (new_location != null) {
                System.out.println("Walking to " + new_location.getLocation().toString());
                sprite_tile.setPosition(new_location);
            }
        }
    }

    public void walkSprite(SpriteCharacter sprite, SpriteCharacter.State state) {
        sprite.goWalking(state);
    }

    public void stopSprite(SpriteCharacter sprite) {
        sprite.stopWalking();
    }

    public void interactSprite(SpriteCharacter sprite, Tile sprite_tile) {
        TileLocation location = sprite_tile.getLocation();
        int x = location.getX(), y = location.getY();

        switch (sprite.getDirection()){
            case Up:
                y = y - 1;
                break;
            case Down:
                y = y + 1;
                break;
            case Left:
                x = x - 1;
                break;
            case Right:
                x = x + 1;
                break;
        }

        System.out.println(new TileLocation(x, y).toString());

        if (x >= 1 && y >= 1 && x <= cols && y <= rows) {
            if (sprite_tile != null) {
                Tile new_location = find(x, y, sprite_tile, true);
                if (new_location != null) {
                    if (new_location.hasAction()){
                        new_location.executeRunnable();
                    }
                }
            }
        }
    }
}
