package com.example.sander.fontysmon;

import android.app.Activity;
import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;

public class Attack {

    enum ID {
        NULL,
        Testguide,
        Discussion,
        Feedback,
        ExpertInterview,
        TrendAnalyses,
        ContextMapping,
        Cardsorting,
        Tinkering,
        UsabilityTest,
        PoC,
        Debug,
        QualityReview,
        LiteratureStudy,
        PaperPrototype,
        None
    }

    enum Type {
        Normal,
        Ux,
        Sco,
        Ded
    }

    private ID id;
    private ID comboBefore;
    private ID comboAfter;
    private String name;
    private String description;
    private Bitmap icon;
    private int minAttack;
    private int maxAttack;
    private Type type;


    private static Map<ID, Attack> attacksList;

    public Attack(ID id, ID comboBefore, ID comboAfter, String name, String description, Bitmap icon, int minAttack, int maxAttack, Type type) {
        this.id = id;
        this.comboBefore = comboBefore;
        this.comboAfter = comboAfter;
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.minAttack = minAttack;
        this.maxAttack = maxAttack;
        this.type = type;
    }

    public Attack(ID id, String name, String description, Bitmap icon, int minAttack, int maxAttack, Type type) {
       this(id, ID.NULL, ID.NULL, name, description, icon, minAttack, maxAttack, type);
    }

    public static void initAttacks(Activity activity) {
        attacksList = new HashMap<>();

        attacksList.put(ID.Testguide, new Attack(ID.Testguide, "Test guide", "-", null, 3, 5, Type.Normal));
        attacksList.put(ID.Discussion, new Attack(ID.Discussion, "Discussion", "-", null, 4, 5, Type.Normal));
        attacksList.put(ID.Feedback, new Attack(ID.Feedback, "Feedback", "-", null, 7, 10, Type.Normal));
        attacksList.put(ID.ExpertInterview, new Attack(ID.ExpertInterview, "Expert interview", "-", null, 5, 10, Type.Sco));
        attacksList.put(ID.TrendAnalyses, new Attack(ID.TrendAnalyses, "Trend Analyses", "-", null, 4, 9, Type.Sco));
        attacksList.put(ID.ContextMapping, new Attack(ID.ContextMapping, "Context Mapping", "-", null, 6, 9, Type.Sco));
        attacksList.put(ID.Cardsorting ,new Attack(ID.Cardsorting, ID.ExpertInterview, ID.Testguide,"Card sorting", "-", null, 7, 11, Type.Ux));
        attacksList.put(ID.Tinkering, new Attack(ID.Tinkering, "Tinkering", "-", null, 8, 12, Type.Ux));
        attacksList.put(ID.UsabilityTest, new Attack(ID.UsabilityTest, "Usability Test", "-", null, 3, 14, Type.Ux));
        attacksList.put(ID.PoC, new Attack(ID.PoC, "Proof of concept", "-", null, 9, 14, Type.Ded));
        attacksList.put(ID.Debug, new Attack(ID.Debug, "Debug", "-", null, 7, 11, Type.Ded));
        attacksList.put(ID.QualityReview, new Attack(ID.QualityReview, "Quality Review", "-", null, 5, 8, Type.Normal));
        attacksList.put(ID.LiteratureStudy, new Attack(ID.LiteratureStudy, "Literature Study", "-", null, 5, 8, Type.Sco));
        attacksList.put(ID.PaperPrototype, new Attack(ID.PaperPrototype, "Paper Prototype", "-", null, 5, 8, Type.Ux));
        attacksList.put(ID.None, new Attack(ID.None, "-", "-", null, 0, 1, Type.Normal));
    }

    public static Attack get(ID id) {
        return attacksList.get(id);
    }
    public ID getId() {
        return id;
    }
    public ID getComboAfter() { return comboAfter; }
    public ID getComboBefore() {return comboBefore; }
    public String getName(){
                                                return name;
    }
    public String getDescription() {
                                                                return description;
    }
    public Bitmap getIcon() {
        return icon;
    }
    public int getMaxAttack() {
        return maxAttack;
    }
    public int getMinAttack() {
        return minAttack;
    }
    public Type getType() {
        return type;
    }

}
