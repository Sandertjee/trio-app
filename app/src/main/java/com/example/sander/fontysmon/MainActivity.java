package com.example.sander.fontysmon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends GameActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button lvlUpBtn = findViewById(R.id.lvlUpBtn);
        Button lvlDownBtn = findViewById(R.id.lvlDownBtn);
        Button healBtn = findViewById(R.id.healBtn);
        Button damageBtn = findViewById(R.id.damageBtn);

//        final List<Attack.ID> methods;

        // create new person with specified parameters
        Person Stefan = new Person("Stefan", 1, 100, null, new ArrayList<Attack.ID>() {{add(Attack.ID.ExpertInterview);add(Attack.ID.Cardsorting);add(Attack.ID.Debug);add(Attack.ID.Testguide);}}, null,  100, 200, false);

        // Get teacher Sebastiaan
        final Person sebastiaan = Person.getTeachers(Person.ID.Sebastiaan);

        // Assign teacher views
        final ImageView teacherimage = findViewById(R.id.teacherImage);
        final TextView teacherName = findViewById(R.id.teacherName);
        final ProgressBar teacherHealth = findViewById(R.id.teacherHealth);
        final TextView teacherLevel = findViewById(R.id.teacherLevel);
        final TextView teacherType = findViewById(R.id.teacherType);


        // set teacher values
        teacherimage.setImageResource(getResources().getIdentifier(sebastiaan.getSprite(), "drawable", getPackageName()));
        teacherName.setText(sebastiaan.getName());
        teacherHealth.setProgress(sebastiaan.getHealth());
        teacherLevel.setText("Level: " + sebastiaan.getLevel());
        teacherType.setText(sebastiaan.getBadges().toArray()[0].toString());

        teacherimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, BattleActivity.class));
            }
        });





        healBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sebastiaan.heal(20);
                teacherHealth.setProgress(sebastiaan.getHealth());

            }
        });

        damageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sebastiaan.damage(Attack.ID.PoC);
                teacherHealth.setProgress(sebastiaan.getHealth());
            }
        });

        lvlUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sebastiaan.levelup();
                teacherLevel.setText("Level: " + sebastiaan.getLevel());
            }
        });

        lvlDownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sebastiaan.levelDown();
                teacherLevel.setText("Level: " + sebastiaan.getLevel());
            }
        });






        // Testing to get data from teacher
//        System.out.println("This should be sebastiaan: " + sebastiaan.getName() + " He's of type " + sebastiaan.getBadges()[0]);

        // testing to get attack data
        System.out.println("The PoC Attack: " + Attack.get(Attack.ID.PoC).getMaxAttack());


    }
}
