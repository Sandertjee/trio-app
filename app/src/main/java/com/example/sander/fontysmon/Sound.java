package com.example.sander.fontysmon;

import android.app.Activity;
import android.media.MediaPlayer;

public class Sound {

    enum Effect {ButtonClick, PlayerDamage, PlayerHit, PlayerWin}

    enum Music {PlayerBattle, MainTheme}

    private Activity activity;
    private MediaPlayer player;

    private Sound(Activity activity){
        this.activity = activity;
    }

    private Sound(Activity activity, Effect effect){
        this(activity);
        init(getResourceID(effect));
    }

    private Sound(Activity activity, Music music){
        this(activity);
        init(getResourceID(music));
    }

    private int getResourceID(Effect effect){
        switch (effect){
            case PlayerHit:
                return R.raw.player_hit;
                default:
            case PlayerDamage:
                return R.raw.player_damage;
            case PlayerWin:
                return R.raw.player_win;
            case ButtonClick:
                return R.raw.button_click_2;
        }

    }

    private int getResourceID(Music music){
        switch (music){
            case PlayerBattle:
                return R.raw.boss_battle;
            default:
                return getResourceID(Effect.PlayerDamage);
        }

    }


    private void init(int resource_id){
        this.player = MediaPlayer.create(activity, resource_id);
    }

    public static Sound play(Activity activity, Effect effect){
        Sound sound = new Sound(activity, effect);
        sound.player.start();
        return sound;
    }

    public static Sound play(Activity activity, Music music){
        Sound sound = new Sound(activity, music);
        sound.player.start();
        return sound;
    }

    public void pause(){
        player.pause();
    }

}













