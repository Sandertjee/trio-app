package com.example.sander.fontysmon;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MapActivity extends GameActivity implements View.OnClickListener, View.OnTouchListener {

    enum Layout {Hallway, ClassRoom}

    MapUI ui;

    RelativeLayout map_layout;
    TextView message_box;
    Button button_up, button_down, button_left, button_right, button_a;
    Bitmap texture_door, texture_floor_orange, texture_floor_yellow, texture_floor_blue, texture_wall, texture_chair, texture_table_corner;
    int color_transparent, color_floor_orange, color_floor_yellow, color_floor_blue, color_wall_grey;
    boolean assessment_started = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        map_layout = findViewById(R.id.map_layout);
        ui = new MapUI(this, map_layout, 10, 8);

        initTextures();
        initDefineButtons();

        initLayoutReset();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int x = ui.getCols() / 2, y = ui.getRows() / 2;
                initLayout(Layout.Hallway, x, y);
            }
        }, 500);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    private void initDefineButtons(){
        message_box = findViewById(R.id.message_box);
        message_box.setVisibility(View.INVISIBLE);

        button_up = findViewById(R.id.button_up);
        button_down = findViewById(R.id.button_down);
        button_left = findViewById(R.id.button_left);
        button_right = findViewById(R.id.button_right);
        button_a = findViewById(R.id.button_a);

        button_up.setOnTouchListener(this);
        button_down.setOnTouchListener(this);
        button_left.setOnTouchListener(this);
        button_right.setOnTouchListener(this);
        button_a.setOnClickListener(this);
    }

    private void initTextures(){
        // Set standard values for the
        color_transparent = ui.getActivity().getColor(R.color.transparent);
        color_floor_orange = this.getColor(R.color.floor_orange);
        color_floor_yellow = this.getColor(R.color.floor_yellow);
        color_floor_blue = this.getColor(R.color.floor_blue);
        color_wall_grey = this.getColor(R.color.wall_grey);

        texture_door = BitmapFactory.decodeResource(getResources(), R.drawable.texture_door);
        texture_floor_orange = BitmapFactory.decodeResource(getResources(), R.drawable.texture_floor_orange);
        texture_floor_yellow = BitmapFactory.decodeResource(getResources(), R.drawable.texture_floor_yellow);
        texture_floor_blue = BitmapFactory.decodeResource(getResources(), R.drawable.texture_floor_blue);
        texture_wall = BitmapFactory.decodeResource(getResources(), R.drawable.texture_wall);
        texture_chair = BitmapFactory.decodeResource(getResources(), R.drawable.texture_chair);
        texture_table_corner = BitmapFactory.decodeResource(getResources(), R.drawable.texture_table_corner);
    }

    private void initLayout(Layout layout, int start_x, int start_y){
        initLayoutReset();
        switch (layout){
            case Hallway:
                initHallwayUI();
                break;
            case ClassRoom:
                initClassUI();
                break;
        }
        setSprite(ui.getPlayerTile(), start_x, start_y);
    }

    private void setSprite(Tile sprite, int x, int y){
        sprite.setBackgroundColor(color_transparent);
        sprite.setLocation(x, y);
        sprite.setPosition(ui.find(x, y, sprite), 0);
    }

    private void initLayoutReset(){
        int black = getColor(R.color.transparent);
        for (Tile tile : ui.getTiles().values()){
            tile.setTileBackground(black);
            tile.resetImage();
            tile.setBlocked(false);
            tile.setOnClickListener(this);
        }
    }

    private void initClassUI(){
        for (Tile tile : this.ui.getTiles().values()){
            new TileEditor(ui, tile).setBackground(texture_floor_blue);
        }

        for (int i = 1; i < ui.getCols() + 1; i++){
            new TileEditor(ui, i, 1).set(texture_wall, true);
            new TileEditor(ui, i, 2).set(texture_wall, true);
            new TileEditor(ui, i, 3).set(texture_wall, true);
        }

        new TileEditor(ui, 6, ui.getRows()).set(BitmapFactory.decodeResource(getResources(), R.drawable.exclamation_mark), new Runnable() {
            @Override
            public void run() {
                initLayout(Layout.Hallway, 2, 3);
            }
        });


        new TileEditor(ui, 3, ui.getRows() - 2).set(texture_floor_blue, SpriteSheet.rotateImage(texture_table_corner, 90), true);
        new TileEditor(ui, 3, ui.getRows() - 1).set(texture_floor_blue, SpriteSheet.rotateImage(texture_table_corner, -90), true);
        new TileEditor(ui, 2, ui.getRows() - 2).setImage(SpriteSheet.flipImage(texture_chair, true));
        new TileEditor(ui, 2, ui.getRows() - 1).setImage(SpriteSheet.flipImage(texture_chair, true));
        new TileEditor(ui, 4, ui.getRows() - 2).setImage(texture_chair);
        new TileEditor(ui, 4, ui.getRows() - 1).setImage(texture_chair);

        final SpriteCharacter seb = new SpriteCharacter(this, R.drawable.sprite_seb, "Seb");
        final Handler handler = new Handler();
        final TileEditor seb_editor = new TileEditor(ui, 4, 4);
        seb_editor.set(texture_floor_blue, seb.getSprite(SpriteCharacter.SpriteState.DownNormal), true, new Runnable() {
            @Override
            public void run() {
                if (!assessment_started){
                    assessment_started = true;
                    final TileEditor edit = new TileEditor(ui, 4, 3);
                    if (ui.getPlayerTile().getLocation().compare(5, 4)){
                        seb_editor.setImage(seb.getSprite(SpriteCharacter.SpriteState.RightNormal));
                    } else if (ui.getPlayerTile().getLocation().compare(3, 4)){
                        seb_editor.setImage(seb.getSprite(SpriteCharacter.SpriteState.LeftNormal));
                    } else {
                        seb_editor.setImage(seb.getSprite(SpriteCharacter.SpriteState.DownNormal));
                    }

                    edit.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.exclamation_mark));
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            edit.resetImage();
                            showMessage("Your Assessment begins now!", 1500);
                        }
                    }, 500);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(getApplicationContext(), BattleActivity.class));
                        }
                    }, 1500);
                }

            }
        });

    }

    private void initHallwayUI(){

        // Set the base color of the floor to yellow
        for (Tile tile : ui.getTiles().values()){
            new TileEditor(ui, tile).setBackground(texture_floor_orange);
        }

        // Create orange panes on the floor
        for (int i = 1; i < ui.getRows() + 1; i++){
            new TileEditor(ui, 4, i).setBackground(texture_floor_yellow);
            new TileEditor(ui, 5, i).setBackground(texture_floor_yellow);
            new TileEditor(ui, 6, i).setBackground(texture_floor_yellow);
        }

        // Build upper walls
        for (int i = 1; i < ui.getCols() + 1; i++){
            new TileEditor(ui, i, 1).set(texture_wall, true);
            new TileEditor(ui, i, 2).set(texture_wall, true);
            new TileEditor(ui, i, 3).set(texture_wall, true);
        }

        // Set doors and actions
        new TileEditor(ui, 2, 3).set(texture_door, true, new Runnable() {
            @Override
            public void run() {
                initLayout(Layout.ClassRoom, 6, ui.getRows() - 1);
            }
        });
        new TileEditor(ui, 6, 3).set(texture_door, true, new Runnable() {
            @Override
            public void run() {
//                Toast.makeText(getApplicationContext(), , Toast.LENGTH_LONG).show();
                showMessage("This door is locked...", 1000);
            }
        });

    }

    private void showMessage(String message, int time){
        message_box.setText(message);
        message_box.setTextSize(17f);
        message_box.setTextColor(Color.parseColor("#1e1e1e"));
        message_box.setTypeface(ResourcesCompat.getFont(this, R.font.eightbitmadness));
        message_box.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                message_box.setText("");
                message_box.setVisibility(View.INVISIBLE);
            }
        }, time);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_a:
                ui.interactSprite(ui.getPlayerSprite(), ui.getPlayerTile());
                break;
            default:
                Tile tile = findViewById(v.getId());
                Toast.makeText(this, "X:"+tile.getLocation().getX()+" Y:"+tile.getLocation().getY(), Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            switch (v.getId()) {
                case R.id.button_up:
                    ui.walkSprite(ui.getPlayerSprite(), SpriteCharacter.State.Up);
                    break;
                case R.id.button_down:
                    ui.walkSprite(ui.getPlayerSprite(), SpriteCharacter.State.Down);
                    break;
                case R.id.button_left:
                    ui.walkSprite(ui.getPlayerSprite(), SpriteCharacter.State.Left);
                    break;
                case R.id.button_right:
                    ui.walkSprite(ui.getPlayerSprite(), SpriteCharacter.State.Right);
                    break;
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            ui.stopSprite(ui.getPlayerSprite());
        }
        return true;
    }
}
