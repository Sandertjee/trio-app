package com.example.sander.fontysmon;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;

public class Tile extends android.support.v7.widget.AppCompatImageView {

    public static int TILE_WIDTH = 180, TILE_HEIGHT = 180;

    private int ID, background_color;
    private Bitmap background_image;
    private TileLocation location;
    private Activity activity;

    private boolean blocked = false, has_action = false;
    private Runnable runnable = null;

    public static void setSize(int size) {
        TILE_WIDTH = TILE_HEIGHT = size;
    }


    public Tile(Activity activity, int ID, int x, int y) {
        this(activity, ID, new TileLocation(x, y));
    }

    public Tile(Activity activity, int ID, TileLocation location) {
        super(activity);

        this.activity = activity;
        this.ID = ID;
        this.setId(ID);
        this.location = location;
        this.setBlocked(false);

        this.setMinimumWidth(TILE_WIDTH);
        this.setMaxWidth(TILE_WIDTH);
        this.setMinimumHeight(TILE_HEIGHT);
        this.setMaxHeight(TILE_HEIGHT);
    }

    public int getID() {
        return ID;
    }

    public void setImage(Bitmap image){
        // §setImageBitmap(image);
        setImageBitmap(Bitmap.createScaledBitmap(image, TILE_WIDTH, TILE_HEIGHT, false));
        this.setScaleType(ScaleType.CENTER_CROP);
    }

    public void resetImage(){
        setImageResource(0);
    }

    public void setTileBackground(int color) {
        this.background_color = color;
        super.setBackgroundColor(color);
    }

    public int getTileBackgroundColor() {
        return background_color;
    }

    public void setTileBackground(Bitmap resource_id) {
        this.background_image = resource_id;
        super.setBackground(new BitmapDrawable(activity.getResources(), Bitmap.createScaledBitmap(resource_id, TILE_WIDTH, TILE_HEIGHT, false)));
    }

    public Bitmap getTileBackgroundImage() {
        return background_image;
    }

    public TileLocation getLocation() {
        return location;
    }

    public void setLocation(int x, int y) {
        this.location.resetCoordinates(x, y);
    }

    public void setPosition(Tile tile){
        long speed = (long) (MapUI.ANIMATION_SPEED * 1.1666666666666666);
        setPosition(tile, speed);
    }
    public void setPosition(Tile tile, long speed){
        setLocation(tile.getLocation().getX(), tile.getLocation().getY());
        this.animate().setDuration(speed).x(tile.getX());
        this.animate().setDuration(speed).y(tile.getY());
    }

    public void setBlocked(boolean blocked){
        this.blocked = blocked;
    }

    public void block(){
        this.setBlocked(true);
    }

    public void unblock(){
        this.setBlocked(false);
    }

    public boolean isBlocked(){
        return blocked;
    }

    public boolean hasAction() {
        return has_action;
    }

    public void setRunnable(Runnable runnable) {
        if (runnable != null) {
            this.has_action = true;
            this.runnable = runnable;
        }
    }

    public void executeRunnable(){
        executeRunnable(0);
    }

    public void executeRunnable(int milliseconds){
        if (runnable != null){
            new Handler().postDelayed(runnable, milliseconds);
        }
    }
}
