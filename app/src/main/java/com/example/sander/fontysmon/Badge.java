package com.example.sander.fontysmon;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.HashMap;
import java.util.Map;

public class Badge {

    enum ID{
        START,
        DED,
        UXU,
        SCO,
        PTM
    }

    private ID id;
    private String name;
    private String description;
    private Bitmap sprite;

    private static Map<ID, Badge> badgeList;


    public Badge(ID id, String name, String description, Bitmap sprite){
        this.id = id;
        this.name = name;
        this.description = description;
        this.sprite = sprite;
    }

    public static void initBadges(Activity activity){
        badgeList = new HashMap<>();
        badgeList.put(ID.DED, new Badge(ID.DED, "DED", "Design And Development", null));
        badgeList.put(ID.UXU, new Badge(ID.UXU, "UXU", "User Experience & User Centered Design", null));
        badgeList.put(ID.SCO, new Badge(ID.SCO, "SCO", "Strategy & Concepting", BitmapFactory.decodeResource(activity.getResources(), R.drawable.badgesco)));
        badgeList.put(ID.PTM, new Badge(ID.PTM, "PTM", "Proftaak Media", null));
    }

    public static Badge get(ID id) { return badgeList.get(id); }

    public String getName(){        return name; }
    public String getDescription(){ return description; }
    public Bitmap getSprite(){      return sprite; }


}
