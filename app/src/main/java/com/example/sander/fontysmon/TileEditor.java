package com.example.sander.fontysmon;

import android.graphics.Bitmap;

public class TileEditor {

    MapUI ui;
    Tile tile;

    public TileEditor(MapUI ui, Tile tile){
        this.ui = ui;
        this.tile = tile;
    }

    TileEditor(MapUI ui, int tile_id){
        this(ui, ui.getTile(tile_id));
    }

    TileEditor(MapUI ui, int x, int y){
        this(ui, ui.find(x, y, true));
    }

    public void set(int background_color, Bitmap image, boolean blocked, Runnable action){
        set(background_color, image, blocked);
        setAction(action);
    }

    public void set(Bitmap background_color, Bitmap image, boolean blocked, Runnable action){
        set(background_color, image, blocked);
        setAction(action);
    }

    public void set(int background_color, Bitmap image, boolean blocked){
        setBackground(background_color);
        setImage(image);
        setBlocked(blocked);
    }

    public void set(Bitmap background_image, Bitmap image, boolean blocked){
        setBackground(background_image);
        setImage(image);
        setBlocked(blocked);
    }

    public void set(final int background_color, boolean blocked, Runnable action){
        set(background_color, blocked);
        setAction(action);
    }

    public void set(Bitmap background_image, boolean blocked, Runnable action){
        set(background_image, blocked);
        setAction(action);
    }

    public void set(int background_color, boolean blocked){
        setBackground(background_color);
        setBlocked(blocked);
    }

    public void set(Bitmap background_image, boolean blocked){
        setBackground(background_image);
        setBlocked(blocked);
    }

    public void set(int background_color, Bitmap image){
        set(background_color, image, false);
    }

    public void set(Bitmap background_image, Bitmap image){
        set(background_image, image, false);
    }

    public void set(int background_color, Runnable action){
        set(background_color, false);
        setAction(action);
    }

    public void set(Bitmap background_image, Runnable action){
        set(background_image, false);
        setAction(action);
    }

    public void setBackground(int color){
        tile.setTileBackground(color);
    }

    public void setBackground(Bitmap bitmap){
        tile.setTileBackground(bitmap);
    }

    public void setImage(Bitmap image){
        tile.setImage(image);
    }

    public void resetImage(){
        tile.setImageResource(0);
    }

    public void setBlocked(boolean blocked){
        tile.setBlocked(blocked);
    }

    public void setAction(Runnable action){
        tile.setRunnable(action);
    }
}
