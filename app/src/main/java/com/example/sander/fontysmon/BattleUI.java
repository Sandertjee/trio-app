package com.example.sander.fontysmon;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


public class BattleUI {

    private GameActivity activity;
    private android.support.v7.widget.GridLayout attackGrid;
    private LinearLayout escapeButtonHolder, messageBox, badgeContainer;
    private TextView attackMessage, playerCurrentHP, playerMaxHP, teacherCurrentHP, teacherMaxHP, playerName, playerLevel, teacherLevel, teacherName, badgeTitle, badgeDescription;
    private ImageView sprite, opponentSprite, badgeIcon;
    private Button badgeConfirm;

    private Battle battleMech;

    private ProgressBar playerHealth, teacherHealth;

    public BattleUI(GameActivity activity) {
        this.activity = activity;
        attackMessage  = new TextView(activity);
        attackGrid = activity.findViewById(R.id.attackGrid);
        escapeButtonHolder = activity.findViewById(R.id.fleeContainer);
        messageBox = activity.findViewById(R.id.messageContainer);
        playerHealth = activity.findViewById(R.id.playerHealthBar);
        teacherHealth = activity.findViewById(R.id.teacherHealthBar);
        playerMaxHP = activity.findViewById(R.id.maxHpPlayer);
        playerCurrentHP = activity.findViewById(R.id.currentHpPlayer);
        teacherMaxHP = activity.findViewById(R.id.maxHpTeacher);
        teacherCurrentHP = activity.findViewById(R.id.currentHpTeacher);
        playerName = activity.findViewById(R.id.playerName);
        playerLevel = activity.findViewById(R.id.playerLevel);
        teacherName = activity.findViewById(R.id.teacherName);
        teacherLevel = activity.findViewById(R.id.teacherLevel);
        badgeIcon = activity.findViewById(R.id.badgeImage);
        badgeTitle = activity.findViewById(R.id.badgeTitle);
        badgeConfirm = activity.findViewById(R.id.resumeGame);
        badgeDescription = activity.findViewById(R.id.badgeDescription);
        badgeContainer = activity.findViewById(R.id.badgeReward);
        badgeConfirm = activity.findViewById(R.id.resumeGame);

        battleMech = new Battle();
    }

    public void buildBattleControls() {

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 0, 10, 20);

        for(int i = 0; i < activity.getUser().getMethods().size(); i++) {
            final Attack attack = Attack.get(activity.getUser().getMethods().get(i));
            final Button attackButton = new Button(activity);
            attackButton.setId(i);
            attackButton.setTag(attack.getId());
            attackButton.setText(attack.getName());
            attackButton.setLayoutParams(params);
            attackButton.setTextColor(Color.WHITE);
            attackButton.getLayoutParams().height = 150;
            attackButton.getLayoutParams().width = 450;
            attackButton.setBackgroundResource(colorSwitch(attack.getType()));
            attackButton.setTypeface(ResourcesCompat.getFont(activity, R.font.eightbitmadness));
            attackButton.setTextSize(17f);
            attackGrid.addView(attackButton);
            attackGrid.setClickable(false);

            attackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println(attackButton.getId());
                    System.out.println(attack.getMaxAttack());
                    System.out.println(attack.getMinAttack());

                    System.out.println(Person.getTeachers(Person.ID.Sebastiaan).getName());
                }
            });
        }

        System.out.println(activity.getUser().getMethods().toString());

        // Escape fight Button

        LinearLayout.LayoutParams escapeParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 0, 10, 0);

        Button escapeButton = new Button(activity);
        escapeButton.setLayoutParams(escapeParams);
        escapeButton.getLayoutParams().height = 150;
        escapeButton.getLayoutParams().width = 920;
        escapeButton.setBackgroundResource(R.drawable.ziekmelden);
        escapeButton.setText("Call in sick");
        escapeButton.setTextSize(17f);
        escapeButton.setTypeface(ResourcesCompat.getFont(activity, R.font.eightbitmadness));
        escapeButton.setTextColor(Color.WHITE);

        escapeButtonHolder.addView(escapeButton);

        escapeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAttackMessage("Seems like you are taking the easy way out! Bye.");

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        activity.finish();
                    }
                }, 1500);
            }
        });

        messageBox.setBackgroundResource(R.drawable.messagebox);
        messageBox.setAlpha(1f);

        attackMessage.setText("Seb challenges you to a fight! Defeat him!");
        attackMessage.setTextSize(17f);
        attackMessage.setTextColor(Color.parseColor("#1e1e1e"));
        attackMessage.setTypeface(ResourcesCompat.getFont(activity, R.font.eightbitmadness));
        attackMessage.setPadding(70, 0, 20, 0);


        messageBox.addView(attackMessage);
    }

    public void initBasics(String name, String level, int maxHealth,  String type) {
        switch(type) {
            case "Player":
                playerName.setText(name);
                playerLevel.setText("Lv"+ level);
                playerHealth.setMax(maxHealth);
                playerHealth.setProgress(maxHealth);
                playerCurrentHP.setText(String.valueOf(maxHealth));
                playerMaxHP.setText("/" + String.valueOf(maxHealth) + " HP");
                break;
            case "Teacher":
                teacherName.setText(name);
                teacherLevel.setText("Lv"+ level);
                teacherHealth.setMax(maxHealth);
                teacherHealth.setProgress(maxHealth);
                teacherCurrentHP.setText(String.valueOf(maxHealth));
                teacherMaxHP.setText("/" + String.valueOf(maxHealth) + " HP");
                break;
        }
    }

    public int colorSwitch(Attack.Type type) {
        switch(type) {
            case Ux:
                return R.drawable.werkplaats;
            case Sco:
                return R.drawable.bieb;
            case Ded:
                return R.drawable.lab;
            default:
                return R.drawable.battleknop;
        }
    }

    public void showAttackMessage(String givenMessage) {
        attackMessage.setText(givenMessage);
    }

    public void setNewHealth(final ProgressBar healthbar, int newHealth, TextView currentHP) {
        int currentHealth = healthbar.getProgress();
        ProgressBarAnimation anim = new ProgressBarAnimation(healthbar, currentHealth, newHealth);
        anim.setDuration(500);
        healthbar.startAnimation(anim);

        currentHP.setText(String.valueOf(newHealth));

        final float healthPercentage = (float) newHealth / healthbar.getMax() * 100;

        if (healthPercentage > 1) {
            healthbar.getProgressDrawable().setColorFilter(Color.parseColor(setProgressColor(healthPercentage)), PorterDuff.Mode.SRC_IN);
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    healthbar.getProgressDrawable().setColorFilter(Color.parseColor(setProgressColor(healthPercentage)), PorterDuff.Mode.SRC_IN);
                }
            }, 500);
        }
    }

    public String setProgressColor(float healthPercentage) {
        if(healthPercentage > 70) {
            return "#1e824c";
        } else if (healthPercentage > 50) {
            return "#e87e04";
        } else if (healthPercentage > 30) {
            return "#f7ca18";
        } else if (healthPercentage > 1) {
            return "#cf000f";
        } else {
            return "#666666";
        }
    }

    public void changeButtonUI(Button button, boolean state) {
        if (state == false) {
            button.setEnabled(false);
            button.setAlpha(0.6f);
        } else {
            button.setEnabled(true);
            button.setAlpha(1f);
        }
    }

    public void battleFinished(Boolean won, String type) {

        if (won == false && type == "Player") {
            showAttackMessage("You failed your assessment, better luck next time!");
            sprite = activity.findViewById(R.id.playerSprite);
            sprite.animate().y(1000f).setDuration(1000);
        } else if (won == true && type == "Teacher") {
            showAttackMessage("You passed your assessment! Good job!");
            sprite = activity.findViewById(R.id.teacherSprite);
            sprite.animate().y(1000f).setDuration(1000);
            Sound.play(activity, Sound.Effect.PlayerWin);
        }
    }

    public void attackAnimation(String type) {
        if (type == "Player") {
            sprite = activity.findViewById(R.id.playerSprite);
            sprite.animate().x(125f).setDuration(200);

            opponentSprite = activity.findViewById(R.id.teacherSprite);
            opponentSprite.animate().alpha(0.2f);
        } else if (type == "Teacher") {
            sprite = activity.findViewById(R.id.teacherSprite);
            sprite.animate().x(-125f).setDuration(200);

            opponentSprite = activity.findViewById(R.id.playerSprite);
            opponentSprite.animate().alpha(0.2f);
        }


        Sound.play(activity, Sound.Effect.PlayerHit);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Sound.play(activity, Sound.Effect.PlayerDamage);
            }
        }, 200);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sprite.animate().x(0f).setDuration(200);
                opponentSprite.animate().alpha(1f);
            }
        }, 100);
    }

    public void buildBadgeReward(Badge.ID badge, Person teacher) {
        badgeIcon.setBackground(new BitmapDrawable(activity.getResources(), Badge.get(badge).getSprite()));
        badgeTitle.setText("You just earned the " + Badge.get(badge).getName() + " Badge");
        badgeDescription.setText("Good job! You passed your assessment with " + teacher.getName() + "! You've earned the " + Badge.get(badge).getName() + " badge! Take good care of it!");
        badgeContainer.setVisibility(View.VISIBLE);

        badgeConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sound.play(activity, Sound.Effect.ButtonClick);
                activity.finish();
            }
        });
    }
}
