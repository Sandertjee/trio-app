package com.example.sander.fontysmon;

import android.app.Activity;
import android.os.Handler;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


public class Battle extends Activity {

    private BattleActivity activity;
    private Person Teacher;
    private Person Player;
    private BattleUI battleUI;
    private Button attackButton;
    private ProgressBar teacherHealth;
    private ProgressBar playerHealth;

    private boolean litStudy;
    private boolean paperProt;
    private boolean usaTest;
    private boolean proofOfC;



    TextView currentHpTeacher;
    TextView currentHpPlayer;

    public Battle(BattleActivity activity, Person Teacher, Person Player, BattleUI battle, ProgressBar teacherHealth, ProgressBar playerHealth) {
        this.activity = activity;
        this.Teacher = Teacher;
        this.Player = Player;
        this.battleUI = battle;
        this.teacherHealth = teacherHealth;
        this.playerHealth = playerHealth;

        currentHpTeacher = this.activity.findViewById(R.id.currentHpTeacher);
        currentHpPlayer = this.activity.findViewById(R.id.currentHpPlayer);

        litStudy = false;
        paperProt = false;
        usaTest = false;
        proofOfC = false;
    }

    public Battle() {

    }

    public void attackTeacher(Attack.ID attackID) {


        switch(attackID){
            case LiteratureStudy:
                litStudy = true;
                paperProt = false;
                usaTest = false;
                proofOfC = false;
                Teacher.damage(attackID, 0);
                battleUI.showAttackMessage("Literature study is a great way to start off your project! very effective.." );
                break;
            case PaperPrototype:
                if(paperProt){
                    litStudy = false;
                    paperProt = false;
                    usaTest = false;
                    proofOfC = false;
                }
                if(litStudy){
                    paperProt = true;
                    Teacher.damage(attackID, 5);
                    battleUI.showAttackMessage("You based your prototype on your previous research! Good job! It was very effective.." );
                    System.out.println("3 point COMBO");
                }
                else{
                    Teacher.damage(attackID, 0);
                    battleUI.showAttackMessage("You can't just make a paper prototype based on nothing! Do some research! Not very effective.." );
                }
                break;
            case UsabilityTest:
                if(usaTest){
                    litStudy = false;
                    paperProt = false;
                    usaTest = false;
                    proofOfC = false;
                }

                if(litStudy && paperProt){
                    usaTest = true;
                    Teacher.damage(attackID, 8);
                    battleUI.showAttackMessage("You usability tested your paper prototype based on your research! It was very effective.." );
                    System.out.println("5 point COMBO");
                }
                else{
                    Teacher.damage(attackID, 0);
                    battleUI.showAttackMessage("There's nothing to test! Make sure you have a prototype ready! Not effective.." );
                }
                break;
            case PoC:
                if(proofOfC){
                    litStudy = false;
                    paperProt = false;
                    usaTest = false;
                    proofOfC = false;
                }
                if(litStudy && paperProt && usaTest){
                    Teacher.damage(attackID, 12);
                    battleUI.showAttackMessage("You based your proof of concept on your previous research results! It was very effective.." );
                    litStudy = false;
                    paperProt = false;
                    usaTest = false;
                    proofOfC = false;
                    System.out.println("7 point COMBO");
                }
                else{
                    Teacher.damage(attackID, 0);
                    battleUI.showAttackMessage("This Proof of Concept has a lot of usability problems! Test it first! Not effective.." );
                }
                break;
        }


        System.out.println(litStudy  + " " + paperProt + " " + usaTest + " " + proofOfC );



//        int combo = Attack.get(previousAttack1).getComboValue(previousAttack2, previousAttack3);
//        Teacher.damage(attackID, 5);

//        battleUI.showAttackMessage("You used " + Attack.get(attackID).getName());
        battleUI.setNewHealth(teacherHealth, Teacher.getHealth(), currentHpTeacher);

        if (Teacher.getHealth() <= 0) {
            battleUI.battleFinished(true, "Teacher");
            badgeReward(Badge.ID.SCO);
            battleUI.setNewHealth(teacherHealth, 0, currentHpTeacher);
        } else {
            battleUI.attackAnimation("Player");
            attackPlayer();
        }
    }

    public void attackPlayer() {

        setButtonState(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Player.damage(Attack.ID.Feedback);
                battleUI.showAttackMessage(Teacher.getName() + " used " + Attack.get(Attack.ID.Cardsorting).getName());
                battleUI.setNewHealth(playerHealth, Player.getHealth(), currentHpPlayer);

                if (Player.getHealth() <= 0) {
                    battleUI.battleFinished(false, "Player");
                    battleUI.setNewHealth(playerHealth, 0, currentHpPlayer);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            activity.finish();
                        }
                    }, 1500);

                } else {
                    battleUI.attackAnimation("Teacher");
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            playerTurn();
                        }
                    }, 1700);
                }
            }
        }, 1700);
    }

    public void playerTurn() {
        setButtonState(true);
        battleUI.showAttackMessage("It's your turn again! better pick a worthy attack!");
    }

    public void setButtonState(boolean state) {
        for (int s = 0; s < 4; s++) {
            attackButton = activity.findViewById(s);
            if (state == false) {
                battleUI.changeButtonUI(attackButton, state);
            } else {
                battleUI.changeButtonUI(attackButton, state);
            }
        }
    }

    public void badgeReward(final Badge.ID badge) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Player.getBadges().add(badge);
                battleUI.buildBadgeReward(badge, Teacher);
            }
        }, 1500);
    }
}
