package com.example.sander.fontysmon;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.HashMap;
import java.util.Map;

public class SpriteCharacter {

    enum State {Up, Down, Left, Right}

    enum SpriteState {
        UpNormal,
        DownNormal,
        LeftNormal,
        RightNormal,
        UpWalk1,
        DownWalk1,
        LeftWalk1,
        RightWalk1,
        UpWalk2,
        DownWalk2,
        LeftWalk2,
        RightWalk2,
    }

    private Bitmap sprite_sheet, current_view;
    private Map<SpriteState, Bitmap> sprite_state;
    private State current_direction = State.Down;
    private String name;
    private boolean is_walking = false, stop_walking = true;

    SpriteCharacter(Activity activity, int id, String name) {
        this(BitmapFactory.decodeResource(activity.getResources(), id), name);
    }

    SpriteCharacter(Bitmap spritesheet, String name) {
        this.sprite_sheet = spritesheet;
        this.sprite_state = new HashMap<>();
        this.name = name;
        this.splitSpriteSheet();
    }

    private void splitSpriteSheet() {
        SpriteSheet sheet = new SpriteSheet(sprite_sheet, 4, 3);

        sprite_state.put(SpriteState.UpNormal, sheet.getSprite(1, 1));
        sprite_state.put(SpriteState.UpWalk1, sheet.getSprite(1, 2));
        sprite_state.put(SpriteState.UpWalk2, sheet.getSprite(1, 3));

        sprite_state.put(SpriteState.DownNormal, sheet.getSprite(2, 1));
        sprite_state.put(SpriteState.DownWalk1, sheet.getSprite(2, 2));
        sprite_state.put(SpriteState.DownWalk2, sheet.getSprite(2, 3));

        sprite_state.put(SpriteState.LeftNormal, sheet.getSprite(3, 1));
        sprite_state.put(SpriteState.LeftWalk1, sheet.getSprite(3, 2));
        sprite_state.put(SpriteState.LeftWalk2, sheet.getSprite(3, 3));

        sprite_state.put(SpriteState.RightNormal, sheet.getSprite(4, 1));
        sprite_state.put(SpriteState.RightWalk1, sheet.getSprite(4, 2));
        sprite_state.put(SpriteState.RightWalk2, sheet.getSprite(4, 3));
    }

    public Bitmap getSprite(SpriteState state) {
        return sprite_state.get(state);
    }

    public Bitmap getCurrentSprite() {
        return current_view;
    }

    public String getName() {
        return name;
    }

    public State getDirection() {
        return current_direction;
    }

    public void setDirection(State current_direction) {
        this.current_direction = current_direction;
    }

    public void setIsWalking(boolean is_walking) {
        this.is_walking = is_walking;
        this.stop_walking = !is_walking;
    }

    public boolean isWalking() {
        return is_walking;
    }

    public void goWalking(State state) {
        this.current_direction = state;
        setIsWalking(true);
    }

    public void stopWalking() {
        setIsWalking(false);
    }
}
