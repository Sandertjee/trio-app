package com.example.sander.fontysmon;

public class TileLocation {

    private int X, Y;

    TileLocation(int X, int Y){
        this.X = X;
        this.Y = Y;
    }

    public int getX() {
        return X;
    }

    public int getY() {
        return Y;
    }

    public boolean compare(int X, int Y){
        return (this.X == X && this.Y == Y);
    }

    public void resetCoordinates(int x, int y){
        this.X = x;
        this.Y = y;
    }

    @Override
    public String toString() {
        return "TileLocation: X:" + X + " x Y:" + Y;
    }
}
