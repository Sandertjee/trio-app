package com.example.sander.fontysmon;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class BattleActivity extends GameActivity implements View.OnClickListener {

    Button attackButton;
    BattleUI battleScreen;
    Battle battleSequence;

    ProgressBar playerHealth;
    ProgressBar teacherHealth;

    Person teacher = Person.getTeachers(Person.ID.Sebastiaan);
    Sound battle_music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battle);

        playerHealth = findViewById(R.id.playerHealthBar);
        teacherHealth = findViewById(R.id.teacherHealthBar);

        battleScreen = new BattleUI(this);

        battleScreen.buildBattleControls();

        for (int s = 0; s < 4; s++) {
            attackButton = findViewById(s);
            attackButton.setOnClickListener(this);
        }

        playBattleMusic();
        battleSequence = new Battle(this, teacher, user, battleScreen, teacherHealth, playerHealth);

        battleScreen.initBasics(teacher.getName(), String.valueOf(teacher.getLevel()), teacher.getHealth(), "Teacher");
        battleScreen.initBasics(user.getName(), String.valueOf(user.getLevel()), user.getHealth(), "Player");
        convertToPx(175);
    }

    private void playBattleMusic(){
        battle_music = Sound.play(this, Sound.Music.PlayerBattle);
    }

    public static float convertToPx(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        System.out.println(Math.round(px));
        return Math.round(px);
    }

    @Override
    protected void onPause() {
        super.onPause();
        battle_music.pause();
    }

    public void buttonSound(){
        Sound.play(this, Sound.Effect.ButtonClick);
    }

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            Button button = findViewById(v.getId());
            battleSequence.attackTeacher(Attack.ID.valueOf(button.getTag().toString()));
        }
    }
}
