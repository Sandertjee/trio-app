package com.example.sander.fontysmon;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

abstract class GameActivity extends AppCompatActivity {
    protected Person user;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialise teachers and attacks
        Attack.initAttacks(this);
        Badge.initBadges(this);
        Person.initTeachers(this);

        // initialise Storage
        Storage storage = new Storage(this);

//        // global teachers
//        final Person sebastiaan = Person.getTeachers(Person.ID.Sebastiaan);


        // load methods
        String methodsTogether = storage.load("Methods", null);
        List methodList = new ArrayList<Attack.ID>();

        if(methodsTogether.contains(",")){
            String[] methodsSeperated = methodsTogether.split(",");

            for(int i = 0; i < methodsSeperated.length; i++){
                methodList.add(Attack.ID.valueOf(methodsSeperated[i]));
            }
        }

        // load badges
        String badgesTogether = storage.load("Badges", null);
        List badgeList = new ArrayList<Badge.ID>();

        if(badgesTogether.contains(",")){
            String[] badgesSeperated = badgesTogether.split(",");

            for(int i = 0; i < badgesSeperated.length; i++){
                badgeList.add(Badge.ID.valueOf(badgesSeperated[i]));
            }
        }

        // initialise the user
        user = new Person(storage.load("Name", "null"), storage.load("level", 1), storage.load("Health", 50), null, methodList, badgeList, 100, 200, false);


        System.out.println("The badges: " + user.getBadges());
        System.out.println("The methods: " + user.getMethods());




    }

    public Person getUser() {
        return user;
    }
}
