package com.example.sander.fontysmon;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashSet;

public class Storage {
    private SharedPreferences data;
    private SharedPreferences.Editor data_editor;
    private static final String FONTYSMON_FILE = "fontysmon";


    Storage(Activity activity){
        data = activity.getSharedPreferences(FONTYSMON_FILE, Context.MODE_PRIVATE);
        data_editor = data.edit();
    }

    void save(String key, String value){    data_editor.putString(key, value).apply();}
    void save(String key, int value){       data_editor.putInt(key, value).apply();}
    void save(String key, double value){    data_editor.putInt(key, (int) Math.ceil(value)).apply();}
    void save(String key, long value){      data_editor.putLong(key, value).apply();}
    void save(String key, boolean value){   data_editor.putBoolean(key, value).apply();}
    void save(String key, float value){     data_editor.putFloat(key, value).apply();}

    String load(String key, String else_return){    return data.getString(key, else_return);}
    int load(String key, int else_return){          return data.getInt(key, else_return);}
    boolean load(String key, boolean else_return){  return data.getBoolean(key, else_return);}
    long load(String key, long else_return){        return data.getLong(key, else_return);}
    float load(String key, float else_return){      return data.getFloat(key, else_return);}

    void clear(){
        data_editor.clear().apply();
        System.out.println("WARNING: Cleared all data!");
    }


}
