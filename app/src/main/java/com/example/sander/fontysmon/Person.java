package com.example.sander.fontysmon;

import android.app.Activity;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Person {

    enum ID{
        Sebastiaan
    }

    private String name;
    private int level;
    private int health;
    private String sprite;
    private List<Attack.ID> methods;
    private List<Badge.ID> badges;
    private double Xposition;
    private double Yposition;
    private boolean isTeacher;

    private static Map<Person.ID, Person> teacherList;

    public Person(String name, int level, int health, String sprite, List methods, List badges, double Xposition, double Yposition, boolean isTeacher) {
        this.name = name;
        this.level = level;
        this.health = health;
        this.sprite = sprite;
        this.methods = methods;
        this.badges= badges;
        this.Xposition = Xposition;
        this.Yposition = Yposition;
        this.isTeacher = isTeacher;

    }

    public static Person getTeachers(Person.ID id) {
        return teacherList.get(id);
    }

    public static void initTeachers(Activity activity){
        teacherList = new HashMap<>();
        teacherList.put(Person.ID.Sebastiaan, new Person("Sebastiaan", 4, 100, "sebastiaan", new  ArrayList<Attack.ID>() {{add(Attack.ID.Feedback);}}, new  ArrayList<Badge.ID>() {{add(Badge.ID.SCO);}}, 300.0, 100.0, true));
    }


    public String getName(){ return name; }

    public int getLevel(){ return level; }

    public int getHealth(){ return health; }

    public String getSprite(){ return sprite; }

    public List<Attack.ID> getMethods(){ return methods; }

    public List<Badge.ID> getBadges(){ return badges; }

    public double getXposition(){ return Xposition; }

    public double getYposition(){ return Yposition; }

    public boolean getIsTeacher(){ return isTeacher; }

    public void setName(String newName){ name = newName;}
    public void setLevel(int newLevel){ level = newLevel;}
    public void levelup(){ level += 1;}
    public void levelDown(){ level -= 1;}
    public void setHealth(int newHealth){ health = newHealth;}
    public void heal(int heal){health += heal;}
    public int damage(Attack.ID attackID){
        Attack attack = Attack.get(attackID);
        int attackDamage = new Random().nextInt(attack.getMaxAttack()) + attack.getMinAttack();
        health -= attackDamage;
        System.out.println("Did " + attackDamage + " New health " + health );
        return attackDamage;
    }

    public int damage(Attack.ID attackID, int combo){
        Attack attack = Attack.get(attackID);
        int attackDamage = new Random().nextInt((attack.getMaxAttack() - attack.getMinAttack()) + 1) + attack.getMinAttack();
        attackDamage += combo;
        health -= attackDamage;
        System.out.println("Did " + attackDamage + " New health " + health + " which included " + combo + " combo damage");
        return attackDamage;
    }



    public void addMethod(Attack.ID attackID){
        methods.add(attackID);
    }
    public void addBadge(Badge.ID badgeID){
        badges.add(badgeID);
    }
    public void setXYposition(int x, int y){ Xposition = x; Yposition = y;}
    public void setTeacher(boolean isNewTeacher){isTeacher = isNewTeacher; }

}
