package com.example.sander.fontysmon;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;

public class SpriteSheet {

    protected Bitmap image;

    private int rows, columns, width, height;

    SpriteSheet(Activity activity, int resource, int rows, int columns){
        this(BitmapFactory.decodeResource(activity.getResources(), resource), rows, columns);
    }

    SpriteSheet(Bitmap image, int rows, int columns){
        this.image = image;
        this.rows = rows;
        this.columns = columns;

        int image_width = image.getWidth();
        int image_height = image.getHeight();

        this.width = image_width / columns;
        this.height = image_height / rows;
    }

    public int getWidth(){ return width; }

    public int getHeight(){ return height; }

    public int getRows(){ return rows; }

    public int getColumns(){ return columns; }

    public Bitmap getSprite(int row, int col){
        return Bitmap.createBitmap(image, ((col - 1) * width), ((row - 1) * height), width, height);
    }

    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0, (bmp1.getHeight() / 2) - bmp2.getHeight() / 2, null);
        return bmOverlay;
    }

    public static Bitmap rotateImage(Bitmap image, int degrees){
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
    }

    public static Bitmap flipImage(Bitmap image, boolean horizontal){
        float cx = image.getWidth() / 2f;
        float cy = image.getHeight()  / 2f;
        int x, y;
        if (horizontal){
            x = -1;
            y = 1;
        } else {
            x = 1;
            y = -1;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(x, y, cx, cy);
        return Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
    }

    public void clear(){
        image.recycle();
    }
}
